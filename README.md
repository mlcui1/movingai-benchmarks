# movingai-benchmarks

Cleaned up grid benchmarks from the 
[Sturtevant's Moving AI](http://movingai.com/benchmarks/)
from the hog2 repository. Used in our paper
[Compromise-free Pathfinding on a Navigation Mesh](http://www.ijcai.org/proceedings/2017/0070.pdf).